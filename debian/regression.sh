#!/bin/sh
# Properly invoke the upstream unit tests.
# This is intended to be run from the same directory as dpkg-buildpackage.
sh ./test_check.sh
PYTHONPATH=`pwd`:$PYTHONPATH python ./test/main.py
